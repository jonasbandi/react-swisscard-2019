const {useState} = React;

function useForm(initialValues, submitHandler){

  const [formValues, setFormValues] = useState(initialValues);

  function handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    setFormValues({...formValues, [name]: value});
  }

  function handleSubmit(e) {
    e.preventDefault();
    submitHandler(formValues)
  }

  return {formValues, handleChange, handleSubmit};
}

function AppComponent() {

  const initialValues = {isGoing: true, numberOfGuests: 0};
  let {formValues, handleChange, handleSubmit} = useForm(initialValues, submitForm);

  // function handleChange(event) {
  //   const target = event.target;
  //   const value = target.type === "checkbox" ? target.checked : target.value;
  //   const name = target.name;
  //
  //   setFormValues({...formValues, [name]: value});
  // }

  function submitForm(formValues) {
    // e.preventDefault();
    console.log(formValues);
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Is going:
        <input
          type="checkbox" name="isGoing"
          checked={formValues.isGoing}
          onChange={handleChange}
        />
      </label>
      <br/>
      <label>
        Number of guests:
        <input
          type="number" name="numberOfGuests"
          value={formValues.numberOfGuests}
          onChange={handleChange}
        />
      </label>
      <br/>
      <button type="submit">Submit</button>
    </form>
  );
}

const app = <AppComponent/>;

ReactDOM.render(app, document.getElementById("root"));


// DEMO:
// create a custom hook `useForm`:
//
// const {formValues, handleChange, handleSubmit} = useForm(initialValues, submitForm);
