import axios from 'axios';

const API_URL = 'http://localhost:3456/todos';

export async function loadToDos(completed = 0) {
  const serverResponse = await axios.get(API_URL, { params: { completed }});
  return serverResponse.data.result;
}

export function saveToDo(toDo) {

    return axios.post(API_URL, toDo)
        .then((response) => response.data.result )
        .catch((error) => {
            console.log(error);

            // DEMO: Do not reload the page, in order to see the effect of the ErrorBoundary
            alert('Something went terribly wrong!');
            window.location.reload();
        });
}

export function updateToDo(toDo) {

    return axios.put(`${API_URL}/${toDo.id}`, toDo)
        .catch((error) => console.log(error));
}

export function deleteToDo(toDo) {

    return axios.delete(`${API_URL}/${toDo.id}`)
        .catch((error) => console.log(error));

}
