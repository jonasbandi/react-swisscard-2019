import React, {useState} from 'react';

export function Greeter() {

  const [name, setName] = useState('World');

  return (
    <div>
      <div>Hello {name}!</div>
      <input data-testid='name-input' onChange={(e) => setName(e.target.value)}/>
    </div>
  )
}
