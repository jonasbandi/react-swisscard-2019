class GreeterComponent extends React.Component {
  render() {
    return (
      <div>
        <h1>Greetings from Class Component</h1>
        <p>Hello World!</p>
        <p>The current time is: {new Date().toLocaleString()}</p>
      </div>
    );
  }
}

// // DEMO: function component
// function GreeterComponent2(){
//   return (
//     <div>
//       <h1>Greetings from Function Component</h1>
//       <p>Hello World!</p>
//       <p>The current time is: {new Date().toLocaleString()}</p>
//     </div>
//   );
// }

ReactDOM.render(
  <div>
    <GreeterComponent/>
  </div>,
  document.getElementById("root")
);

// DEMO:
// On class component:
// - extract app-component into variable
// - introduce variables in render for message & date
// - conditional rendering of message
// - State: introduce state for date & refresh button
// - Props: greeting messages as props
// - Lifecycle: schedule update in componentDidMount
//
// Introduce function component:
// - State: introduce state for date & refresh button
// - Props: greeting messages as props
// - Lifecycle: schedule update with useEffect
// - Show refresh of component in dev tools
// - extract sub-component
// - introduce React.memo
// - Checkbox for showing the time: `<input type="checkbox" checked={} onChange={} /> Show Time`
// - introduce React.useCallback
