import * as React from 'react';
import ToDoList from './ToDoList';
import { IToDo } from '../model/ToDo';
import {loadToDos, deleteToDo} from '../model/persistence';

type PendingScreenState = {
    todos: IToDo[];
}

class DoneToDos extends React.Component<{}, PendingScreenState> {

  state: PendingScreenState = {
    todos: []
  };

  render() {
    return (
      <div>
        <div className="main">
          <ToDoList todos={this.state.todos} onRemoveToDo={this.removeToDo}/>
        </div>
      </div>
    );
  }

  async componentDidMount() {
    const doneToDos = await loadToDos(1);
    this.setState({todos: doneToDos});
  }

  removeToDo = (toDo: IToDo) => {

    // "OPTIMISTIC UI"
    const newToDos = this.state.todos.filter(t => t.id !== toDo.id);
    this.setState({todos: newToDos});

    deleteToDo(toDo);
  };

  updateToDos = (updatedToDos: IToDo[]) => {
    const doneToDos = updatedToDos.filter(t => t.completed)
    this.setState({todos: doneToDos});
  };
}

export default DoneToDos;