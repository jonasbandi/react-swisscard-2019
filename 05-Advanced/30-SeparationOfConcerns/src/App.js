import React from 'react';
import './App.css';
import PictureViewer from './PictureViewer/PictureViewer';


function App() {
  return (
    <PictureViewer/>
  );
}

export default App;
