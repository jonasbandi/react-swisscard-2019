class FirstComponent extends React.Component {
    render() {
        return (
            <div className="green">
                <h1>Hello from React class component!</h1>
            </div>
        );
    }
}

function SecondComponent(){
    return (
      <div className="green">
          <h1>Hello from React function component!</h1>
      </div>
    )
}

const snippet = <FirstComponent/>;
// let snippet = <SecondComponent/>;

ReactDOM.render(
    snippet,
    document.getElementById('component3')
);


// DEMO:
// - Pass message as prop to component
// - Add nested content
// - Remove nested content and add content as children prop
// - Create props object and spread it into JSX declaration
