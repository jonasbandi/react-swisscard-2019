import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ToDoStore from './model/ToDoStore';
import {Provider} from 'mobx-react';

const toDoStore = new ToDoStore();

// DEMO:
//MOBX_STORE.loadedToDos.push({title: 'Relax, its responsive ...'})
//MOBX_STORE.loadedToDos.get(0).title = 'Test'
//window['MOBX_STORE'] = toDoStore;

ReactDOM.render(
  <Provider toDoStore={toDoStore}>
    <App/>
  </Provider>,
  document.getElementById('root')
);
